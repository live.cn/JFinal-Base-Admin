package web.service;

import java.util.List;

import core.Const;
import web.model.AdminUser;
import web.model.Permissions;
import web.model.Roles;

public class AdminService {
	public List<AdminUser> getList() {
		return AdminUser.dao.find("select * from admin_user");
	}

	public AdminUser getUserByName(String username) {
		return AdminUser.dao.findFirstByCache(Const.ADMIN_USER_CACHE, Const.ADMIN_USER_CACHE + username,
				"select * from admin_user where username  = ?", username);
	}

	public List<Roles> getRoles(Long adminid) {
		List<Roles> roles = Roles.dao
				.findByCache(Const.ADMIN_ROLE_CACHE, Const.ADMIN_ROLE_CACHE + "-" + adminid,
						"select * from roles where id in (SELECT role_id FROM admin_role WHERE admin_id=?)", adminid);
		return roles;
	}

	public List<Permissions> getPermission(Long adminid) {
		List<Permissions> perms = Permissions.dao.findByCache(Const.ADMIN_PERM_CACHE,
				Const.ADMIN_PERM_CACHE + "-" + adminid,
				"select * from permissions where id in (SELECT perm_id FROM admin_perm WHERE admin_id=?)", adminid);
		return perms;
	}
}
