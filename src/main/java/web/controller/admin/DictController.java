package web.controller.admin;

import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

import core.Const;
import kit.StringUtil;
import plugin.route.ControllerBind;
import web.common.BaseController;
import web.common.JsonEntity;
import web.common.enums.CacheNameEnum;
import web.common.enums.ResultCodeEnum;
import web.model.Dict;
/**
 * 字典表
 * 
 */
@ControllerBind(controllerKey = "/admin/dict")
public class DictController extends BaseController {

	private static final String path = "dict_";
	private final static String CONTROLLER_KEY = "/admin/dict";

	public void list() {
		Dict model = getModel(Dict.class,"attr");
		int pageindex = getParaToInt("page", 1);
		StringBuilder sql  = new StringBuilder("FROM dict  where 1=1 ");
		if (StringUtil.isNotBlank(model.getValue())) {
			sql.append(" and value like '%").append(StringUtil.toSqlLike(model.getValue())).append("%' ");
		}
		if (StringUtil.isNotBlank(model.getLabel())) {
			sql.append(" and label like '%").append(StringUtil.toSqlLike(model.getValue())).append("%' ");
		}
		if (StringUtil.isNotBlank(model.getType())) {
			sql.append(" and type like '%").append(StringUtil.toSqlLike(model.getValue())).append("%' ");
		}
		sql.append(" ORDER BY type, sort, modify_time  DESC");
		Page<Dict> page = Dict.dao.paginate(pageindex, Const.PAGE_SIZE, "SELECT *",
				sql.toString());
		setAttr("page", page);
		setAttr("attr", model);
		render(path + "list.ftl");
	}

	public void add() {
		render(path + "add.ftl");
	}

	public void view() {
		Dict model = Dict.dao.findById(getParaToLong(0, 0L));
		setAttr("model", model);
		render(path + "view.ftl");
	}

	public void status() {
		long id = getParaToLong(0, 0L);
		JsonEntity<Dict> json = new JsonEntity<>(ResultCodeEnum.ERROR);
		if (id != 0) {
			Dict d = Dict.dao.findById(id);
			if (d != null) {
				Dict temp = new Dict();
				temp.setId(d.getId());
				if (d.getDelFlag().intValue() == 0) {
					temp.setDelFlag(1);
				} else {
					temp.setDelFlag(0);
				}
				if (temp.update()) {
					CacheKit.remove(CacheNameEnum.DICT.getName(), CacheNameEnum.DICT.getMapName());
					json = new JsonEntity<>(ResultCodeEnum.SUCCESS);
					json.setData(temp);
				}
			}
		}
		renderJson(json);
	}
	public void delete() {
		long id = getParaToLong(0, 0L);
		if (id != 0) {
			boolean r = new Dict().deleteById(id);
			if (r) {
				CacheKit.remove(CacheNameEnum.DICT.getName(), CacheNameEnum.DICT.getMapName());
				sendAlert("删除成功", CONTROLLER_KEY + "/list");
			} else {
				sendAlert("删除失败", CONTROLLER_KEY + "/list");
			}
		} else {
			sendAlert("删除失败", CONTROLLER_KEY + "/list");
		}
	}

	public void edit() {
		long id = getParaToLong(0, 0L);
		if (id > 0) {
			Dict model = Dict.dao.findById(id);
			setAttr("model", model);
			render(path + "edit.ftl");
		} else {
			sendAlert("操作失败:未找到id", CONTROLLER_KEY + "/list");
		}
	}

	public void save() {
		long pid = getParaToLong(0, 0L);
		Dict model = getModel(Dict.class,"model");
		boolean result =false;
		if (pid > 0) { // 更新
			result =	model.update();
		} else { // 新增
			model.remove("id");
			result=	model.save();
		}
		if (result) {
			CacheKit.remove(CacheNameEnum.DICT.getName(), CacheNameEnum.DICT.getMapName());
			sendAlert("保存成功", CONTROLLER_KEY + "/list");
		} else {
			sendAlert("保存失败", CONTROLLER_KEY + "/list");
		}
	}
}
