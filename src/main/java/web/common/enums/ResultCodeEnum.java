package web.common.enums;


public enum ResultCodeEnum {
	SUCCESS(200, "成功"), 
	ERROR(400, "失败"), 
	FETCHRET_ERROR(10100,"文件拉取失败"), 
	SEND_MESSAGE_ERROR(10200,"发送消息失败"),
	PARAM_ERROR(10300, "参数提交错误"), 
	RESULT_EMPTY(10400, "结果为空");

	private String message;
	private int code;

	private ResultCodeEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public int getCode() {
		return code;
	}

}
